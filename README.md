# What is this?

This detects and ships ssh fingerprints as DNS SSHFP records to cloudflare.

# How to use

1. `go get -u gitlab.com/worr/sshfp`
2. `sshfp --apikey <cloudflare api key> --email `<cloudflare email>`
