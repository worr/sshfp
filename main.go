package main

import (
	"bufio"
	"github.com/cloudflare/cloudflare-go"
	"github.com/mkideal/cli"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var (
	DEBUG = false
)

const SSH_KEYGEN = "ssh-keygen"

type argT struct {
	cli.Helper
	ApiKey    string `cli:"apikey" usage:"apikey for interacting with cloudflare"`
	Email     string `cli:"email" usage:"Email address"`
	Debug     bool   `cli:"debug" usage:"turn on debug logging"`
	Hostname  string `cli:"hostname" usage:"Optional hostname (detected by default)"`
	SSHKeygen string `cli:"ssh-keygen" usage:"Optional ssh-keygen binary to use instead of deteching from $PATH"`
	TTL       int    `cli:"ttl" usage:"Optional TTL for SSHFP records"`
	Zone      string `cli:"zone" usage:"Optional zone name (detected by default)"`
}

func debug(msg string) {
	if DEBUG {
		log.Print(msg)
	}
}

func debugf(msg string, rest ...interface{}) {
	if DEBUG {
		log.Printf(msg, rest...)
	}
}

func findZone(cf *cloudflare.API, zoneName string, hostname string) string {
	if zoneName == "" {
		zoneName = strings.SplitN(hostname, ".", 2)[1]
		debugf("Detected zone name as %q", zoneName)
	}

	if ret, err := cf.ZoneIDByName(zoneName); err != nil {
		log.Fatalf("Could not find zone %q in cloudflare: %v", zoneName, err.Error())

		// unreachable
		return ""
	} else {
		debugf("zoneid: %q", ret)
		return ret
	}
}

func findSSHKeygen(providedPath string) string {
	if providedPath != "" {
		debugf("Using provided path: %q", providedPath)
		return providedPath
	}

	if path, err := exec.LookPath(SSH_KEYGEN); err != nil {
		log.Fatalf("Could not find ssh-keygen in $PATH: %v", err.Error())

		// unreachable
		return ""
	} else {
		debugf("Found ssh-keygen at %q", path)
		return path
	}
}

func getHostname(providedHostname string) string {
	if providedHostname != "" {
		debugf("Using provided hostname: %q", providedHostname)
		return providedHostname
	}

	if hostname, err := os.Hostname(); err != nil {
		log.Fatalf("No hostname provided, and not able to detect hostname: %v", err.Error())

		// unreachable
		return ""
	} else {
		debugf("Discovered hostname %q", hostname)
		return hostname
	}
}

func makeDNSRecord(hostname string, algo, hash uint, fp string, ttl int) *cloudflare.DNSRecord {
	data := make(map[string]interface{})
	data["algorithm"] = algo
	data["type"] = hash
	data["fingerprint"] = fp

	record := new(cloudflare.DNSRecord)
	record.Type = "SSHFP"
	record.Name = hostname
	record.Data = data

	if ttl != 0 {
		record.TTL = ttl
	}

	debugf("%+v", *record)
	return record
}

func getSSHFPRecords(sshKeygen, hostname string, ttl int) []*cloudflare.DNSRecord {
	debug(hostname)
	cmd := exec.Command(sshKeygen, "-r", hostname)
	stdioFd, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)

		// unreachable
		return nil
	}

	debugf("Running %v -r %v", sshKeygen, hostname)
	if err := cmd.Start(); err != nil {
		log.Fatalf("Could not run ssh-keygen: %v", err.Error())

		// unreachable
		return nil
	}

	stdio := bufio.NewReader(stdioFd)

	ret := make([]*cloudflare.DNSRecord, 0)
	for line, err := stdio.ReadString('\n'); err == nil; line, err = stdio.ReadString('\n') {
		debug(line)
		parts := strings.Split(line, " ")
		if len(parts) != 6 {
			log.Fatal("Unexpected output from ssh-keygen!")
		}

		if rrclass := parts[1]; rrclass != "IN" {
			log.Fatalf("Invalid rrclass, expected IN, got %q", rrclass)
		}

		if rrtype := parts[2]; rrtype != "SSHFP" {
			log.Fatalf("Invalid rrtype, expected SSHFP, got %q", rrtype)
		}

		rawalgo, err := strconv.ParseUint(parts[3], 10, 8)
		if err != nil {
			log.Fatalf("Invalid algorithm %q", parts[3])
		}
		algo := uint(rawalgo)

		rawhash, err := strconv.ParseUint(parts[4], 10, 8)
		if err != nil {
			log.Fatalf("Invalid hash %q", parts[4])
		}
		hash := uint(rawhash)

		fp := parts[5]

		ret = append(ret, makeDNSRecord(hostname, algo, hash, fp, ttl))
	}

	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}

	return ret
}

func main() {
	os.Exit(cli.Run(new(argT), func(ctx *cli.Context) error {
		argv := ctx.Argv().(*argT)

		if argv.Debug {
			DEBUG = true
		}

		sshKeygen := findSSHKeygen(argv.SSHKeygen)
		hostname := getHostname(argv.Hostname)
		ttl := argv.TTL

		zone := argv.Zone

		cf, err := cloudflare.New(argv.ApiKey, argv.Email)
		if err != nil {
			return err
		}

		zoneID := findZone(cf, zone, hostname)

		records := getSSHFPRecords(sshKeygen, hostname, ttl)

		for _, record := range records {
			if _, err := cf.CreateDNSRecord(zoneID, *record); err != nil {
				return err
			}
		}

		return nil
	}))
}
